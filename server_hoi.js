const { Client } = require('pg');
const express = require("express");
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser'); //recover parameters URL

//app.use(express.json());
app.use(cors());

// recover parameters URL
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const client = new Client({
    user: "postgres",
    password: "password",
    host: "localhost",
    port: 5432,
    database: "myDB"
})

const hostname = 'localhost';
const port = 5000;


// give CORS access to data got by the server to my front-end site
app.all('/', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
    next()
});


/** Display info from DB */
app.get("/escaperooms", async(req, res) => {
    try {
        const infoER = await ERData();
        res.setHeader("content-type", "application/json");
        res.send(JSON.stringify(infoER));
    } catch (e) {
        console.error(`app get error: ${e}`)
    }
})

/** Display info from DB for the homepage*/
app.get("/home", async(req, res) => {
    try {
        const dataPB = await PBdata();
        res.setHeader("content-type", "application/json");
        res.send(JSON.stringify(dataPB));
    } catch (e) {
        console.error(`app get error: ${e}`)
    }
})

function createQueryArray(array){
    let myoption = '';
    let myfilter = '';

    if (array.length === 1 && array[0].length < 1){
        // no filter tag, no where clause needed
        myoption = "noFilter";
    } else if (array.length === 1 && array[0].length > 1){
        // only 1 filter tag
        myoption = "oneFilter";
        myfilter = myfilter.concat("'%", array[0], "%'");
    } else if (array.length > 1){
        myoption = "moreFilter";
        array.forEach(function(element, index) {
            if (index === 0 ){
                myfilter = myfilter.concat("photo_tags like '%", element, "%'");
            } else {
                myfilter = myfilter.concat(" and photo_tags like '%", element, "%'");
            }
        });
    }
    return [myoption, myfilter];
}

/** Display info from DB for selected photos by tag*/
app.get("/photoselection", async(req, res) => {
    const rawtag = req.query.search;
    console.log("rawtag: ", rawtag);
    /** 1 unique tag for filtering */
    //const filters = "'%" + rawtag + "%'";
    /** array of tags for filtering */
    const arrayTags = rawtag.split(',');
    const option = createQueryArray(arrayTags)[0];
    const filters = createQueryArray(arrayTags)[1];

    try {
        const selectedPhotos = await somePhotos(option, filters);
        res.setHeader("content-type", "application/json");
        res.send(JSON.stringify(selectedPhotos));
    } catch (e) {
        console.error(`app get error: ${e}`)
    }
})

/** Insert/Update DB for Escaperooms */
app.post("/escaperooms", async(req, res) => {
    let result = {};
    try {
        const reqJson = req.body;
        await insertData(reqJson.toInsert);
        result.success = true;
    } catch (e) {
        console.error(`app post error: ${e}`);
        result.success = false;
    } finally {
        res.setHeader("content-type", "application/json");
        res.send(JSON.stringify(result));
    }
})

/** get tags for filtering the photos */
app.post("/photoselection", async(req, res) => {
    let tags = {};
    try {
        const reqJson = req.body;
        await insertData(reqJson.toInsert);
        console.log(tags)
        tags.success = true;
    } catch (e) {
        console.error(`app post error: ${e}`);
        tags.success = false;
    } finally {
        res.setHeader("content-type", "application/json");
        res.send(JSON.stringify(tags));
    }
})

app.listen(port, () => console.log(`web listening in port ${port}`))

start()

async function start() {
    try {
        await connect();
    } catch (e) {
        console.error(`Start error: ${e}`)
    }
}

async function connect() {
    try {
        await client.connect()
    } catch (e) {
        console.error(`Failed to connect: ${e}`)
    }
}


async function ERData() {
    try {
        const results = await client.query('select "ID_er", date_er, name_er, company_er, players_er, "Photos_blog".photo_path from "EscapeRooms" left join "Photos_blog" on "EscapeRooms"."photoPath_er"="Photos_blog"."ID_photo" order by "ID_er" desc');
        console.table(results.rows)
        return results.rows;
    } catch (e) {
        console.error(`Display Escape Rooms data error: ${e}`);
        return []
    }
}

async function somePhotos(option, keyWord) {
    let personallyzedQuery = ``;
    if (option === "noFilter"){
        personallyzedQuery = `select "ID_photo" as id, photo_path as src, photo_width as width, photo_height as height from public."Photos_blog" order by "ID_photo" desc limit 50`;
    } else if (option === "oneFilter") {
        personallyzedQuery = `select "ID_photo" as id, photo_path as src, photo_width as width, photo_height as height from public."Photos_blog" where photo_tags like ${keyWord} order by "ID_photo" desc`;
    } else if (option === "moreFilter"){
        personallyzedQuery = `select "ID_photo" as id, photo_path as src, photo_width as width, photo_height as height from public."Photos_blog" where ( ${keyWord} ) order by "ID_photo" desc`;
    }
    try {
        const results = await client.query(personallyzedQuery);
        //console.table(results.rows)
        return results.rows;
    } catch (e) {
        console.error(`Display somePhotos data error: ${e}`);
        return []
    }
}

// array where photos are arranged by title
let arrangedPhotoBlogEntries = [];

function addEntry(photo_title, photo_text, photos){
    arrangedPhotoBlogEntries.push({photo_title, photo_text, photos});
}

async function PBdata() {

    const query_get_last_entries = 
`
SELECT 
    "Entries"."title_SP" AS photo_title,
    "ps"."ps" AS photo_text,
	"photos"
FROM 
-- aggregate all photos together per title
-- only keep last N titles
(
	SELECT 
		photo_title as title_id,
		json_agg(
		   json_build_object(
			   'id', "ID_photo",
		   		'src', photo_path,
			   'width', photo_width,
			   'height', photo_height
		   )
		) as photos
	FROM "Photos_blog"
	GROUP BY photo_title
	ORDER BY photo_title DESC
	LIMIT 5
) pb
INNER JOIN
-- aggregate all the paragraphs together per title (id)
(
	SELECT 
		"Description"."title_fKey" as title_id,
		array_agg("Description"."description_SP") AS ps
	FROM "Description"
	GROUP BY "Description"."title_fKey"
) ps 
	ON ps."title_id" = pb."title_id"
-- join with the table that contains the title (as text, not as id)
INNER JOIN "Entries"
    ON ps."title_id" = "Entries"."ID_entry"
ORDER BY "Entries"."ID_entry" DESC;
	
`;
    try {
        const results = await client.query(query_get_last_entries);
        console.table(results.rows)     
        return results.rows;
    } catch (e) {
        console.error(`Display photoblog error: ${e}`);
        return []
    }
}